function mapObject(object,callback){

    let kindOfobject = typeof object;
    let kindOfFunc = typeof callback;
    if(kindOfobject === "object" && kindOfFunc === "function")
    {   for (let index = 0; index < Object.values(object).length; index++) {
            object[Object.keys(object)[index]] = callback(Object.values(object)[index],Object.keys(object)[index])
    }
        return object;
    }
    else
    {
        return null;
    }


}

module.exports = mapObject;