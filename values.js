function values(object)
{
    let kindOf = typeof object
    
    if(kindOf === 'object'){
    let objectInArray = Object.entries(object)
    let result = [];

    for (let index = 0; index < objectInArray.length; index++) {
        result.push(objectInArray[index][1])
    }
        return result
}
else{
    return null;
}
    }

module.exports = values;