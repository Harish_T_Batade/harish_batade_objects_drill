
function keys(object)
{
    let kindOf = typeof object
    
    if(kindOf === 'object'){
    let objectInArray = Object.entries(object)
    let result = [];

    for (let index = 0; index < objectInArray.length; index++) {
        result.push(objectInArray[index][0])
    }
        return result
}
else{
    return null;
}
    }

module.exports = keys;