
function defaults(object , defaultProps){

    let kindOfobject = typeof object;
    let result = Object.assign(object, defaultProps);
    
    if(kindOfobject === "object")
    {   
    return result;
    }
    else
    {
        return null;
    }

}

module.exports =defaults;