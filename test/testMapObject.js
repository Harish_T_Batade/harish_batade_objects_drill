const mapObject = require("../mapObject");

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

function cb(value,key)
{
    let result = typeof value
    return result;
}

let result = mapObject(testObject,cb)

let originalResult = mapObject(testObject,function cb(value,key)
{
    let result = typeof value
    return result;
})

console.log(result)

console.log(originalResult)

if(result==null || originalResult==null)
{
    console.log("you have entered invalid argument the map function works only on object")
}
else if(!compare(result.toString(),originalResult.toString()))
{
    console.log("your map function works")
}


function compare(result,originalResult)
{
    
    let length=0;
    let situation = false;
    if(result.length!=originalResult.length)
    {
        situation = true;
    }
    else
    {
        length=Math.max(result.length,originalResult.length)

        for (let index = 0; index < length; index++) {
            if(result[index]!=originalResult[index])
            {
                situation = true;
                break;
            }

            
        }

    }
    return situation;
}