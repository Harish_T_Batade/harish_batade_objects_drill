const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

let values = require('../values')

let result = values(testObject)

let originalResult = Object.values(testObject)
console.log(result)
console.log(originalResult)
if(result==null || originalResult.length == 0)
{
    console.log("you have entered invalid argument the values function works only on object")
}
else if(!compare(result.toString(),originalResult.toString()))
{
    console.log("your values function works")
}


function compare(result,originalResult)
{
    
    let length=0;
    let situation = false;
    if(result.length!=originalResult.length)
    {
        situation = true;
    }
    else
    {
        length=Math.max(result.length,originalResult.length)

        for (let index = 0; index < length; index++) {
            if(result[index]!=originalResult[index])
            {
                situation = true;
                break;
            }

            
        }

    }
    return situation;
}