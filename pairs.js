function pairs(obj)
{
    let result = [];
    let kindofobject = typeof obj;
if(kindofobject === "object")
{
    for (let index = 0; index < Object.keys(obj).length; index++) {
        result.push([Object.keys(obj)[index], Object.values(obj)[index]])
    }
    return result
}
else
{
    return null
}
}

module.exports = pairs;